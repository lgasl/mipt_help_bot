use concurrency_manager::Manager;
use regex::{Captures, Regex};
use std::process::Command as ProcessCommand;
use std::sync::mpsc::Receiver;
use std::sync::Arc;
use telegram_bot::types::*;
use telegram_bot::{CanReplySendMessage, CanSendMessage};

use command::{AccessFlag, Command};
use command_tools::*;
use db;
use sender::MessageSender;

pub fn add_commands() -> Vec<Command> {
    let mut commands: Vec<Command> = Vec::new();
    // TODO add commands
    // Common
    commands.push(Command::new("start", r"", true, AccessFlag::Any, menu));
    commands.push(Command::new("help", r"", false, AccessFlag::Any, help));
    commands.push(Command::new(
        "reg",
        r"",
        true,
        AccessFlag::Any,
        add_scholar_dialog,
    ));
    commands.push(Command::new(
        "reg_tutor",
        r"",
        true,
        AccessFlag::Any,
        add_tutor_dialog,
    ));
    commands.push(Command::new("menu", r"", true, AccessFlag::Any, menu));

    // Scholar
    commands.push(Command::new("want", r"", true, AccessFlag::Scholar, want));

    // Tutor
    commands.push(Command::new(
        "set_active",
        r"",
        false,
        AccessFlag::Tutor,
        set_active,
    ));
    commands.push(Command::new(
        "set_inactive",
        r"",
        false,
        AccessFlag::Tutor,
        set_inactive,
    ));
    commands.push(Command::new(
        "can_teach",
        r"",
        true,
        AccessFlag::Tutor,
        can_teach,
    ));

    // Admin
    commands.push(Command::new(
        "change_rights",
        r"(?P<id>\d+) (?P<rights>\d+)",
        false,
        AccessFlag::Admin,
        change_rights,
    ));
    commands.push(Command::new("tr", r"", true, AccessFlag::Admin, tr));
    commands.push(Command::new("ls", r"", false, AccessFlag::Admin, ls));

    commands
}

fn help(user_id: UserId, _args: Captures, _update: Update, _receiver: Option<Receiver<Update>>) {
    let mut sender = MessageSender::new();
    let chat: ChatId = user_id.into();

    let help_text = format!("Если ваше общение с ботом зашло в тупик, нажмите /menu. Если это не помогло, нажмите /break.
    Если Вам нужна помощь с учебой, нажмите “зарегистрироваться, как ученик” и следуйте инструкциям на экране. Как только Вы зарегистрируетесь, нажмите “запрос урока”. Уроки бывают по разным предметам, так что выбирайте название предмета, наиболее близкое к Вашей проблеме.
    Если Вы хотите помогать людям с учебой, нажмите “зарегистрироваться, как преподаватель” и следуйте инструкциям. Если вы взяли контакты ученика, но урок сорвался, пишите в отчете “время урока в часах “, равное нулю.
    
Дисклеймер: Мы не поддерживаем скатывание домашних заданий и просьбы решить кому-либо ДЗ/контрольные. Пожалуйста, не пользуйтесь нашей системой, если вы хотите, чтобы вам удаленно решили экзамен/контрольную, или целиком решили домашнее задание. Также просим не накручивать себе рейтинги и не проводить фейковые занятия. К нарушителям могут применяться санкции.
");
    sender.spawn(chat.text(help_text));
}

fn menu(user_id: UserId, args: Captures, update: Update, receiver: Option<Receiver<Update>>) {
    let receiver = match receiver {
        Some(rx) => rx,
        _ => panic!("dialog function haven't receiver"),
    };
    let mut sender = MessageSender::new();
    let menu_reply = get_menu(user_id.clone());
    sender.spawn(menu_reply);

    let reply = receiver.recv().unwrap();
    let (message_id, reply_text) = match get_query_message_and_data(&reply) {
        Some((message, data)) => (message.id, data),
        None => return,
    };

    let action = match reply_text.as_ref() {
        "reg_tutor" => add_tutor_dialog,
        "reg_scholar" => add_scholar_dialog,
        "want" => want,
        _ => {
            sender.spawn(DeleteMessage::new(user_id, message_id));
            return;
        }
    };
    sender.spawn(DeleteMessage::new(user_id, message_id));

    action(user_id, args, update, Some(receiver));
}

// name: &str, telegram_id: i64, faculty: i16,
// group: i16, site: &str
fn add_scholar_dialog(
    user_id: UserId,
    _args: Captures,
    update: Update,
    receiver: Option<Receiver<Update>>,
) {
    let receiver = match receiver {
        Some(rx) => rx,
        _ => panic!("dialog function haven't receiver"),
    };
    let mut sender = MessageSender::new();
    let chat: ChatId = user_id.into();
    let telegram_name = match &update.kind {
        UpdateKind::Message(message) => &message.from.first_name,
        _ => panic!("invalid message"),
    };

    let (name, telegram_id, faculty, group, _site) =
        match get_user_info_dialog(user_id.clone(), &receiver, &mut sender) {
            Some(value) => value,
            None => return,
        };

    if check_user_info(&chat, &name, faculty, group, &receiver, &mut sender) {
        match db::add_scholar(&name, faculty, group, telegram_id, &telegram_name) {
            Ok(_) => sender.spawn(
                chat.text("Вам успешно добавлена роль ученика"),
            ),
            Err(_) => {
                sender.spawn(chat.text("Попытка добавления Вам роли ученика не удалась. \
                                                Пожалуйста обратитесь к администратору"));
                return;
            }
        }

        if db::add_user(telegram_id, AccessFlag::Scholar).is_err() {
            db::add_rights(telegram_id, AccessFlag::Scholar);
        }
    }
}

fn add_tutor_dialog(
    user_id: UserId,
    _args: Captures,
    update: Update,
    receiver: Option<Receiver<Update>>,
) {
    let receiver = match receiver {
        Some(rx) => rx,
        _ => panic!("dialog function haven't receiver"),
    };
    let mut sender = MessageSender::new();
    let chat: ChatId = user_id.into();
    let telegram_name = match &update.kind {
        UpdateKind::Message(message) => &message.from.first_name,
        _ => panic!("invalid message"),
    };

    let (name, telegram_id, faculty, group, _site) =
        match get_user_info_dialog(user_id.clone(), &receiver, &mut sender) {
            Some(value) => value,
            None => return,
        };

    if check_user_info(&chat, &name, faculty, group, &receiver, &mut sender) {
        match db::add_tutor(&name, faculty, group, telegram_id, &telegram_name) {
            Ok(_) => sender.spawn(chat.text("Вам успешно добавлена роль преподавателя, \
                                        пожалуйста, выберите преподаваемые дисциплины")),
            Err(_) => {
                sender.spawn(chat.text("Попытка добавления Вам роли преподавателя не удалась. \
                                                Пожалуйста обратитесь к администратору"));
                return;
            }
        }

        if db::add_user(telegram_id, AccessFlag::Tutor).is_err() {
            db::add_rights(telegram_id, AccessFlag::Tutor);
        }
    }

    let telegram_id: Integer = user_id.into();
    let subjects = get_subjects(chat, &receiver, &mut sender);
    for subject in subjects {
        check_db_result(db::tutor_canteach(telegram_id, subject as i16));
    }
}

fn ls(user_id: UserId, _args: Captures, _update: Update, _receiver: Option<Receiver<Update>>) {
    let mut sender = MessageSender::new();
    let chat: ChatId = user_id.into();
    let output = ProcessCommand::new("ls").arg("-lh").output();
    match output {
        Ok(output) => {
            let info = String::from_utf8_lossy(&output.stdout);
            sender.spawn(chat.text(format!("{}", info)));
        }
        Err(err) => sender.spawn(chat.text(format!("{}", err))),
    }
}

fn tr(user_id: UserId, _args: Captures, _update: Update, receiver: Option<Receiver<Update>>) {
    let receiver = match receiver {
        Some(rx) => rx,
        _ => panic!("dialog function haven't receiver"),
    };
    let mut sender = MessageSender::new();
    let chat: ChatId = user_id.into();
    sender.spawn(chat.text(format!("Hi! Waiting reply")));

    let update = receiver.recv().unwrap();
    if is_break(&update) {
        return;
    }

    if let Some((message, _data)) = get_text_message(&update) {
        sender.spawn(message.text_reply(format!("{}, reply received", &message.from.first_name)));
    } else {
        sender.spawn(chat.text(format!("Non-text message received")));
    }
}

fn set_active(
    user_id: UserId,
    _args: Captures,
    _update: Update,
    _receiver: Option<Receiver<Update>>,
) {
    check_db_result(db::set_tutor_active(Integer::from(user_id)));
}

fn set_inactive(
    user_id: UserId,
    _args: Captures,
    _update: Update,
    _receiver: Option<Receiver<Update>>,
) {
    check_db_result(db::set_tutor_inactive(Integer::from(user_id)));
}

fn can_teach(
    user_id: UserId,
    _args: Captures,
    _update: Update,
    receiver: Option<Receiver<Update>>,
) {
    let receiver = match receiver {
        Some(value) => value,
        _ => panic!("dialog function haven't receiver"),
    };

    let mut sender = MessageSender::new();
    let subjects = get_subjects(ChatId::from(user_id), &receiver, &mut sender);
    for subject in subjects {
        check_db_result(db::tutor_canteach(i64::from(user_id), subject as i16));
    }
}

fn change_rights(
    user_id: UserId,
    args: Captures,
    _update: Update,
    _receiver: Option<Receiver<Update>>,
) {
    let id = match args["id"].parse::<i64>() {
        Ok(value) => value,
        Err(_) => {
            let mut sender = MessageSender::new();
            sender.spawn(ChatId::from(user_id).text("Не правильно задан id"));
            return;
        }
    };
    let rights = match args["rights"].parse::<u8>() {
        Ok(value) => value as i16,
        Err(_) => {
            let mut sender = MessageSender::new();
            sender.spawn(
                ChatId::from(user_id)
                    .text("Не правильно задан параметр прав"),
            );
            return;
        }
    };

    db::change_rights(id, rights);
}

fn want(user_id: UserId, _args: Captures, _update: Update, receiver: Option<Receiver<Update>>) {
    let receiver = match receiver {
        Some(rx) => rx,
        _ => panic!("dialog function haven't receiver"),
    };
    let mut sender = MessageSender::new();
    let chat: ChatId = user_id.into();

    let sections: &Vec<Section> = &load_sections();
    let keyboard: InlineKeyboardMarkup = get_sections_keyboard(sections);
    let keyboard_clone = keyboard.clone();
    let message: SendMessage =
        keyboard_with_text(&chat, "Выберите раздел", keyboard_clone);
    sender.spawn(message);

    let mut stage = 0;
    let mut chosen_subject: u8 = 0;
    while stage != 2 {
        let update = receiver.recv().unwrap();
        if is_button_break(&update) || is_break(&update) {
            if let Some(message) = get_query_message(&update) {
                sender.spawn(DeleteMessage::new(chat, &message));
            }
            return;
        }
        if is_button_back(&update) {
            if let Some(message) = get_query_message(&update) {
                let keyboard_clone = keyboard.clone();
                sender.spawn(get_edit_keyboard(
                    &chat,
                    &message.to_message_id(),
                    "Выберите раздел",
                    keyboard_clone,
                ));
                stage = 0;
            }
            continue;
        }

        if let Some((message, data)) = get_query_message_and_data(&update) {
            if stage == 0 {
                let group: usize = match data.parse() {
                    Ok(value) => value,
                    Err(_) => continue,
                };
                let subjects = &sections[group].sub_themes;

                let mut keyboard: InlineKeyboardMarkup = get_subjects_keyboard(subjects);
                sender.spawn(get_edit_keyboard(
                    &chat,
                    &message.to_message_id(),
                    "Выберите предмет",
                    keyboard,
                ));
                stage = 1;
                continue;
            } else if stage == 1 {
                chosen_subject = match data.parse() {
                    Ok(value) => value,
                    Err(_) => continue,
                };
                sender.spawn(DeleteMessage::new(chat, &message));
                stage = 2;
            }
        } else {
            continue;
        }
    }

    sender.spawn(chat.text(format!("Пожалуйста, опишите подробнее тему, которую Вы хотите разобрать")));
    let mut info: String = String::new();
    while info.is_empty() {
        let update = receiver.recv().unwrap();
        if is_break(&update) {
            return;
        }

        if let Some(data) = get_text(&update) {
            info = data.clone();
        } else {
            continue;
        }
    }

    let keyboard = get_ok_cancel_keyboard();
    let subjects = load_subjects_map();
    let subject_name = match subjects.get(&chosen_subject) {
        Some(value) => value,
        None => panic!("invalid subject id"),
    };
    let text: String = format!("Проверьте правильность данных и нажмите Ok, если всё верно:\n\
                                предмет:{}, описание:{}",
                               subject_name, info);
    sender.spawn(keyboard_with_text(&chat, &text, keyboard));

    loop {
        let update = receiver.recv().unwrap();
        if is_button_break(&update) || is_break(&update) {
            if let Some(message) = get_query_message(&update) {
                sender.spawn(DeleteMessage::new(chat, &message));
            }
            return;
        }

        if is_button_confirmation(&update) {
            let telegram_id: Integer = user_id.into();
            let lesson_id = match db::add_lesson(telegram_id, chosen_subject as i16) {
                Ok(value) => value,
                _ => return,
            };

            if let Some(message) = get_query_message(&update) {
                let text: String = format!("Ваш заказ предложен преподавателям: предмет:{}, описание:{},\
                                            номер заказа:{}", subject_name, info, lesson_id);
                sender.spawn(get_edit_empty_keyboard(
                    &chat,
                    &message.to_message_id(),
                    &text,
                ));
            }

            let tutors = match db::get_tutors(chosen_subject as i16) {
                Ok(value) => value,
                _ => return,
            };
            for tutor in tutors {
                sender.spawn(get_lesson_offer(
                    UserId::new(tutor),
                    subject_name,
                    &info,
                    lesson_id,
                ))
            }

            return;
        }
    }
}

// catch lesson acc/rej, report
pub fn catch_callback_query(update: Update, manager: Arc<Manager>) {
    if let Some((user_id, message, data)) = get_query_from_and_message_and_data(&update) {
        println!("query: <{}>", data);
        let regex = Regex::new(r"(?P<command>\w{3})\s*(?P<id>\d+)").unwrap();
        let captures = regex.captures(data);
        match captures {
            Some(captures) => {
                let mut sender = MessageSender::new();
                let chat_id: ChatId = message.chat.id();
                let lesson_id: i32 = captures["id"].parse().unwrap();
                let telegram_id: Integer = user_id.into();

                match captures["command"].as_ref() {
                    "acc" => {
                        let get_lesson_stage = match db::get_lesson_stage(lesson_id) {
                            Ok(value) => value,
                            _ => return,
                        };

                        if get_lesson_stage == 0 {
                            let (messages, tutors) = match db::get_check(lesson_id) {
                                Ok(value) => value,
                                _ => return,
                            };
                            remove_checked_messages(tutors, messages, &mut sender);

                            check_db_result(db::lesson_update1(lesson_id as i32, telegram_id));
                            let request = get_lesson_tutors_report_req(user_id, lesson_id);
                            sender.spawn(request);

                            check_db_result(db::remove_check(lesson_id as i32));
                        };
                    }
                    "rej" => {
                        sender.spawn(DeleteMessage::new(chat_id, message.id));
                    }
                    "che" => {
                        let get_lesson_stage = match db::get_lesson_stage(lesson_id as i32) {
                            Ok(value) => value,
                            Err(_) => {
                                slog_error!(slog_scope::logger(), "Check of invalid lesson";
                                            "tutor" => telegram_id, "lesson_id" => lesson_id,
                                            );
                                return;
                            }
                        };

                        if get_lesson_stage != 0 {
                            sender.spawn(DeleteMessage::new(chat_id, message.id));
                        } else {
                            // TODO change lesson_id to i64
                            let raw_message_id: Integer = message.id.into();
                            let raw_user_id: Integer = user_id.into();
                            check_db_result(db::add_check(
                                lesson_id as i32,
                                raw_message_id,
                                raw_user_id,
                            ));

                            check_lesson_offer(
                                user_id,
                                message.id,
                                chat_id,
                                lesson_id,
                                &mut sender,
                            );
                        };
                    }
                    // tre ~ tutor report
                    "tre" => {
                        let receiver = manager.build_channel(&user_id);
                        get_tutor_report(lesson_id as i32, chat_id, &receiver, &mut sender);

                        let scholar: UserId = UserId::new(db::get_scholar_id_by_lesson(lesson_id));
                        let chat: ChatId = scholar.into();
                        sender.spawn(get_lesson_scholar_report_req(&chat, lesson_id));
                        manager.remove_key(&user_id);
                    }
                    // ure ~ user report
                    "ure" => {
                        let receiver = manager.build_channel(&user_id);
                        get_scholar_report(lesson_id as i32, chat_id, &receiver, &mut sender);

                        manager.remove_key(&user_id);
                    }
                    &_ => panic!("try_catch: invalid callback query"),
                }
            }
            None => (),
        }
    } else {
        slog_error!(slog_scope::logger(), "got invalid callback query");
    }
}
