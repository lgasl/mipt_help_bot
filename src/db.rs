use dotenv::dotenv;

use std::fs::OpenOptions;
use futures::Stream;
use slog::Drain;
use tokio_core::reactor::Core;
//use telegram_bot::*;
//use std::time::Duration;
use std::sync::mpsc::{Receiver};
use std::sync::Arc;
use std::thread;
use std::env;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use diesel::insert_into;
use diesel::result::Error;
use diesel::expression_methods::*;
use chrono::NaiveDateTime;




pub fn establish_connection() -> PgConnection {
    let database_url = env::var("DATABASE_SOCKET")
        .expect("DATABASE_SOCKET must be set");
    let connection = PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url));
    slog_info!(slog_scope::logger(), "connected to database");
    connection
}




use models::*;
use command::AccessFlag;


pub fn add_tutor<'a>(name1: &'a str, faculty1: i16, group1: i16, telegram_id1: i64, telegram_name1: &'a str
) -> Result<(), Error> {
    use schema::tutors::dsl::*;
    let conn = establish_connection();



    slog_info!(slog_scope::logger(), "add_tutor";
                "name" => name1,"faculty" => faculty1,  "group" => group1,"telegram_id" => telegram_id1);

    match insert_into(tutors).values((name.eq(name1), faculty.eq(faculty1), group.eq(group1),
                                      telegram_id.eq(telegram_id1),telegram_name.eq(telegram_name1))
    )
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "tutor_added_successfully"; "telegram_id" => telegram_id1);
            Ok(())
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "Unable to add tutor";
                        "name" => name1, "telegram_id" => telegram_id1,
                        "faculty" => faculty1, "group" => group1, "error" => format!("{}", error),
                        );
            // should be more informative
            Err(error)
        },
    }

}


pub fn set_tutor_inactive(telegram_id1: i64)
    -> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    let target = tutors.filter(telegram_id.eq(telegram_id1));
    match diesel::update(target)
        .set(active.eq(false))
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "tutor deactivated"; "telegram_id" => telegram_id1);
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "tutor deactivation failed";
                         "telegram_id" => telegram_id1, "error" => format!("{}", error)
                        );
            // should be more informative
            return Err(error)
        }
    };
    Ok(())
}


pub fn set_tutor_active(telegram_id1: i64)
    -> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    let target = tutors.filter(telegram_id.eq(telegram_id1));
    match diesel::update(target)
        .set(active.eq(true))
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "tutor reactivated"; "telegram_id" => telegram_id1);
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "tutor reactivation failed";
                         "telegram_id" => telegram_id1, "error" => format!("{}", error)
                        );
            // should be more informative
            return Err(error)
        }
    };
    Ok(())
}


pub fn add_scholar<'a>(name1: &'a str, faculty1: i16, group1: i16, telegram_id1: i64, telegram_name1: &'a str
) -> Result<(), Error> {
    use schema::scholars::dsl::*;
    let conn = establish_connection();
    let sc1=Scholar{
        name: name1,
        faculty: faculty1,
        group: group1,
        telegram_id: telegram_id1,
        telegram_name: telegram_name1,
    };

    slog_info!(slog_scope::logger(), "add_scholar";
                "name" => name1,"faculty" => faculty1,  "group" => group1,"telegram_id" => telegram_id1);

    match insert_into(scholars).values((name.eq(sc1.name), faculty.eq(sc1.faculty), group.eq(sc1.group),
                                      telegram_id.eq(sc1.telegram_id),telegram_name.eq(sc1.telegram_name)
    ))
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "scholar_added_successfully"; "telegram_id" => telegram_id1);
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "Unable to add scholar";
                        "name" => name1, "telegram_id" => telegram_id1,
                        "faculty" => faculty1, "group" => group1, "error" => format!("{}", error)
                        );
            // should be more informative
            return Err(error)
        },
    };
    Ok(())
}


//may be unsafe
pub fn tutor_canteach(telegram_id1: i64, new_subj1: i16)
    -> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    let loc= tutors.select(list_of_courses)
        .filter(telegram_id.eq(telegram_id1))
        .load::<Vec<i16>>(&conn)
        ;
    let mut a:Vec<i16>=vec![];
    for i in &loc {
        for j in i{
            for k in j{
                a.push(*k);
                }
            }
    }
    if a.contains(&new_subj1){
    }
    else {a.push(new_subj1);}



    match  diesel::update(tutors.filter(telegram_id.eq(telegram_id1)))
        .set(list_of_courses.eq(a))
        .execute(&conn)
        {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "added subject"; "telegram_id" => telegram_id1, "subject"=> new_subj1);
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "adding subject failed";
    "telegram_id" => telegram_id1, "subject"=> new_subj1, "error" => format!("{}", error)
    );
            // should be more informative
            return Err(error)
        }
    };
    Ok(())
}


pub fn duty_fulfilled(telegram_id1: i64)-> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    match  diesel::update(tutors.filter(telegram_id.eq(telegram_id1)))
        .set(experience.eq(experience+1))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), "added 1 exp"; "telegram_id" => telegram_id1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "adding 1 exp failed";
    "telegram_id" => telegram_id1, "error" => format!("{}", error)
    );
                return Err(error)
            }
        };
    Ok(())
}


pub fn update_scholar_site(telegram_id1: i64, site1: &str)-> Result<(), Error>
{
    use schema::scholars::dsl::*;
    let conn = establish_connection();
    match  diesel::update(scholars.filter(telegram_id.eq(telegram_id1)))
        .set(site.eq(site1))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), "updated scholar site"; "telegram_id" => telegram_id1,"site" => site1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "updating scholar site failed";"telegram_id" => telegram_id1, "error" => format!("{}", error));
                return Err(error)
            }
        };
    Ok(())
}


pub fn update_scholar_mail(telegram_id1: i64, mail1: &str)-> Result<(), Error>
{
    use schema::scholars::dsl::*;
    let conn = establish_connection();
    match  diesel::update(scholars.filter(telegram_id.eq(telegram_id1)))
        .set(mail.eq(mail1))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), "updated scholar mail"; "telegram_id" => telegram_id1,"mail" => mail1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "updating scholar mail failed";"telegram_id" => telegram_id1,"mail" => mail1, "error" => format!("{}", error));
                return Err(error)
            }
        };
    Ok(())
}


pub fn update_tutor_site(telegram_id1: i64, site1: &str)-> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    match  diesel::update(tutors.filter(telegram_id.eq(telegram_id1)))
        .set(site.eq(site1))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), "updated tutor site"; "telegram_id" => telegram_id1,"site" => site1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "updating tutor site failed";"telegram_id" => telegram_id1, "error" => format!("{}", error));
                return Err(error)
            }
        };
    Ok(())
}


pub fn update_tutor_mail(telegram_id1: i64, mail1: &str)-> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    match  diesel::update(tutors.filter(telegram_id.eq(telegram_id1)))
        .set(mail.eq(mail1))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), "updated tutor mail"; "telegram_id" => telegram_id1,"mail" => mail1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "updating tutor mail failed";"telegram_id" => telegram_id1,"mail" => mail1, "error" => format!("{}", error));
                return Err(error)
            }
        };
    Ok(())
}


pub fn update_tutor_recos(telegram_id1: i64, new_subj1: i16 )
                      -> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    let loc= tutors.select(list_of_recos)
        .filter(telegram_id.eq(telegram_id1))
        .load::<Vec<i16>>(&conn);
    let mut a:Vec<i16>=vec![];
    for i in &loc {
        for j in i{
            for k in j{
                a.push(*k);
            }
        }
    }
    if a.contains(&new_subj1){}
        else {a.push(new_subj1);}

    match  diesel::update(tutors.filter(telegram_id.eq(telegram_id1)))
        .set(list_of_recos.eq(a))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), "added subject"; "telegram_id" => telegram_id1, "subject"=> new_subj1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "adding subject failed";
    "telegram_id" => telegram_id1, "subject"=> new_subj1, "error" => format!("{}", error)
    );
                // should be more informative
                return Err(error)
            }
        };
    Ok(())
}


//unsafe, addlogs
fn return_scholar_id_corresponding_to_scholar_telegram_id(scholar_id2: i64)->i32{
    let conn = establish_connection();
    use schema::scholars::dsl::*;
    scholars.select(id)
        .filter(telegram_id.eq(scholar_id2))
        .load::<i32>(&conn).unwrap().pop().unwrap()


}



pub fn add_lesson(scholar_telegram_id1: i64, subject1: i16)
    -> Result<i32, Error>
{
    use schema::lessons::dsl::*;
    let conn = establish_connection();
    let scholar_id1= return_scholar_id_corresponding_to_scholar_telegram_id(scholar_telegram_id1);
    let inserted_row: Result<Lesson, Error> = insert_into(lessons)
        .values((scholar_id.eq(scholar_id1), subject.eq(subject1)))
        .get_result(&conn);
    match inserted_row {
        Ok(a) => {slog_info!(slog_scope::logger(), "lesson added"; "scholar_id" => scholar_id1,"subject1" => subject1);
            Ok(a.id)}
        ,
        Err(error) => {
            slog_error!(slog_scope::logger(), "lesson NOT added"; "scholar_id" => scholar_id1,"subject1" => subject1, "error" => format!("{}", error),);

            Err(error)},
    }
}


//unsafe, addlogs
fn return_tutor_id_corresponding_to_repet_telegram_id_(tutor_telegram_id2: i64)->i32{
    let conn = establish_connection();
    use schema::tutors::dsl::*;
    tutors.select(id)
        .filter(telegram_id.eq(tutor_telegram_id2))
        .load::<i32>(&conn).unwrap().pop().unwrap()


}


pub fn lesson_update1(id1:i32,repet_telegram_id1:i64)-> Result<(), Error>{
    use schema::lessons::dsl::*;
    let conn = establish_connection();
    let repet_id1=return_tutor_id_corresponding_to_repet_telegram_id_(repet_telegram_id1);
    match diesel::update(lessons.filter(id.eq(id1)))
        .set((repet_id.eq(repet_id1),stage.eq(1)))
        .execute(&conn) {
        Ok(_) => {slog_info!(slog_scope::logger(), "updated lesson to stage 1"; "lesson_id" => id1,"repet_id" => repet_id1);
            Ok(())
        },
        Err(error) => {slog_error!(slog_scope::logger(), "failed to update lesson to stage 1"; "lesson_id" => id1,"repet_id" => repet_id1, "error" => format!("{}", error));

            Err(error)},
    }

}


pub fn lesson_update2(lesson_id1:i32,date1:NaiveDateTime,length1:f32)-> Result<(), Error>{
    use schema::lessons::dsl::*;
    let conn = establish_connection();
    match diesel::update(lessons.filter(id.eq(lesson_id1)))
        .set((date.eq(date1),length.eq(length1),stage.eq(2)))
        .execute(&conn) {
        Ok(_) => {slog_info!(slog_scope::logger(), "updated lesson to stage 2"; "lesson_id" => lesson_id1, "date" => date1.to_string(),"length" => length1);
            Ok(())
        }
        ,
        Err(error) => {slog_error!(slog_scope::logger(), "failed to update lesson to stage 2"; "lesson_id" => lesson_id1, "date" => date1.to_string(),"length" => length1, "error" => format!("{}", error));
            Err(error)
        },
    }

}


pub fn get_lesson_stage(id1:i32)-> Result<i16, Error>{
    use schema::lessons::dsl::*;
    let conn = establish_connection();
    let loc = lessons.select(stage)
        .filter(id.eq(id1))
        .load::<Option<i16>>(&conn);
    match loc {
        Ok(_) => {slog_info!(slog_scope::logger(), "lesson stage queried"; "lesson_id" => id1);
            Ok(loc.unwrap().pop().unwrap().unwrap())
        },
        Err(error) => {slog_error!(slog_scope::logger(), "failed to update lesson to stage 1"; "lesson_id" => id1, "error" => format!("{}", error));

            Err(error)},
    }
   // std::result::Result<std::vec::Vec<_>, diesel::result::Error>
}
//


pub fn add_check(lesson_id1:i32,messages1:i64,tutors1:i64)->Result<(),Error> {
    use schema::checks::dsl::*;
    let conn = establish_connection();
    let loc1= checks.select(messages)
        .filter(lesson_id.eq(lesson_id1))
        .load::<Option<Vec<i64>>>(&conn)
        .unwrap();
    let mut a:Vec<i64>=vec![];
    for i in &loc1 {
        for j in i{
            for k in j{
                a.push(*k);
            }
        }
    }
    a.push(messages1);
    let loc2= checks.select(tutors)
        .filter(lesson_id.eq(lesson_id1))
        .load::<Option<Vec<i64>>>(&conn)
        .unwrap();
    let mut b:Vec<i64>=vec![];
    for i in &loc2 {
        for j in i{
            for k in j{
                b.push(*k);
            }
        }
    }
    b.push(tutors1);
    match insert_into(checks)
        .values((lesson_id.eq(lesson_id1), messages.eq(&a), tutors.eq(&b)))
        .on_conflict(lesson_id)
        .do_update()
        .set((messages.eq(&a), tutors.eq(&b)))
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "added check"; "lesson_id"=>lesson_id1,"messages"=>messages1,"tutors"=>tutors1);
            Ok(())
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "failed to add check"; "lesson_id"=>lesson_id1,"messages"=>messages1,"tutors"=>tutors1, "error" => format!("{}", error));
            Err(error)
        }
    }
}

pub fn add_checks(lesson_id1:i32,messages1:Vec<i64>,tutors1:Vec<i64>)->Result<(),Error> {
    use schema::checks::dsl::*;
    let conn = establish_connection();
    let loc1= checks.select(messages)
        .filter(lesson_id.eq(lesson_id1))
        .load::<Option<Vec<i64>>>(&conn)
        .unwrap();
    let mut a:Vec<i64>=vec![];
    for i in &loc1 {
        for j in i{
            for k in j{
                a.push(*k);
            }
        }
    }
    a.extend_from_slice(&messages1);

    let loc2= checks.select(tutors)
        .filter(lesson_id.eq(lesson_id1))
        .load::<Option<Vec<i64>>>(&conn)
        .unwrap();
    let mut b:Vec<i64>=vec![];
    for i in &loc2 {
        for j in i{
            for k in j{
                b.push(*k);
            }
        }
    }
    b.extend_from_slice(&tutors1);
    match insert_into(checks)
        .values((lesson_id.eq(lesson_id1), messages.eq(&a), tutors.eq(&b)))
        .on_conflict(lesson_id)
        .do_update()
        .set((messages.eq(&a), tutors.eq(&b)))
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "added checks"; "lesson_id"=>lesson_id1);
            Ok(())
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "failed to add checks"; "lesson_id"=>lesson_id1, "error" => format!("{}", error));
            Err(error)
        }
    }
}


pub fn get_check(lesson_id1:i32)->Result<(Vec<i64>,Vec<i64>),Error> {
    use schema::checks::dsl::*;
    let conn = establish_connection();
    let loc1= checks.select(messages)
        .filter(lesson_id.eq(lesson_id1))
        .load::<Option<Vec<i64>>>(&conn);
    match loc1 {
        Err(error)=> {slog_error!(slog_scope::logger(), "failed to get checks"; "lesson_id"=>lesson_id1, "error" => format!("{}", error));
            return Err(error)},
        Ok(_) =>{
            let loc2= checks.select(tutors)
                .filter(lesson_id.eq(lesson_id1))
                .load::<Option<Vec<i64>>>(&conn);
            match loc2 {
                Err(error)=> {slog_error!(slog_scope::logger(), "failed to get check"; "lesson_id"=>lesson_id1, "error" => format!("{}", error));
                    return Err(error)},
               Ok(_)=> {
                   slog_info!(slog_scope::logger(), "get check"; "lesson_id"=>lesson_id1);
                   return Ok((loc1.unwrap().pop().unwrap().unwrap(),loc2.unwrap().pop().unwrap().unwrap() ))}

            }


        }
    }



}


pub fn remove_check(lesson_id1:i32)->Result<(),Error> {
    use schema::checks::dsl::*;
    let conn = establish_connection();
    match diesel::delete(checks.filter(lesson_id.eq(lesson_id1)))
        .execute(&conn) {
        Ok(_) => {
            slog_info!(slog_scope::logger(), "remove check"; "lesson_id"=>lesson_id1);
            Ok(())
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "failed to remove check"; "lesson_id"=>lesson_id1, "error" => format!("{}", error));
            Err(error)
        }
    }



}



pub fn get_tutors(subject1:i16)
    ->Result<Vec<i64>,Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();

    match tutors.select(telegram_id)
        .filter(list_of_courses.overlaps_with(vec!(subject1)))
        .filter(active.eq(true))
        .load(&conn) {
        Ok(a) => {
            slog_info!(slog_scope::logger(), "remove check"; "subject"=>subject1);
            Ok(a)
        },
        Err(error) => {
            slog_error!(slog_scope::logger(), "failed to get tutors"; "subject"=>subject1, "error" => format!("{}", error));
            Err(error) }
    }
}



pub fn get_user_access_rights(telegram_id1:i64)-> i16 {
    use schema::users::dsl::*;
    let conn = establish_connection();
    // let r1= users.select().filter(telegram_id.eq(telegram_id1)).load::<User>(&conn);
    let res = users.select(groups)
        .filter(telegram_id.eq(telegram_id1))
        .load::<i16>(&conn)
        .unwrap();
    if res.len() == 0 {
        return 0;
    }
    res[0]
}


//unsafe,tests needed,addlogs
pub fn add_user(telegram_id1:i64, new_rights1: AccessFlag) -> Result<usize, Error>{
    use schema::users::dsl::*;
    let conn = establish_connection();
    let rights: i16 = new_rights1  as i16;
    slog_info!(slog_scope::logger(), "add_user";
                "telegram_id" => telegram_id1,"rights"=> rights);

    insert_into(users).values((telegram_id.eq(telegram_id1),groups.eq(rights))).execute(&conn)

}


//unsafe,tests needed,addlogs
pub fn add_rights(telegram_id1:i64, new_rights1: AccessFlag) {
    use schema::users::dsl::*;
    let conn = establish_connection();
    let rights: i16 = new_rights1  as i16;
    let oldrights:i16= users.select(groups)
        .filter(telegram_id.eq(telegram_id1))
        .load(&conn).unwrap().pop().unwrap();
    let new_rights=rights|oldrights;
    slog_info!(slog_scope::logger(), "change_rights";
                "telegram_id" => telegram_id1,"rights"=> rights);
     diesel::update(users.filter(telegram_id.eq(telegram_id1)))
        .set(groups.eq(new_rights))
        .execute(&conn);
}


//unsafe,tests needed,addlogs
pub fn change_rights(telegram_id1:i64, rights1: i16) {
    use schema::users::dsl::*;
    let conn = establish_connection();
    slog_info!(slog_scope::logger(), "add_rights";
                "telegram_id" => telegram_id1,"rights"=> rights1);
    diesel::update(users.filter(telegram_id.eq(telegram_id1)))
        .set(groups.eq(rights1))
        .execute(&conn);
}


//unsafe,tests needed,addlogs
pub fn remove_rights(telegram_id1:i64, new_rights1: AccessFlag) {
    use schema::users::dsl::*;
    let conn = establish_connection();
    let rights: i16 = new_rights1  as i16;

    let oldrights:i16= users.select(groups)
        .filter(telegram_id.eq(telegram_id1))
        .load(&conn).unwrap().pop().unwrap();
    let rights=!rights;
    let new_rights=rights&oldrights;
    slog_info!(slog_scope::logger(), "remove_rights";
                "telegram_id" => telegram_id1,"rights"=> rights);
    diesel::update(users.filter(telegram_id.eq(telegram_id1)))
        .set(groups.eq(new_rights))
        .execute(&conn);
}


//unsafe, addlogs
fn return_corresponding_tutor_id(lesson_id2:i32)->i32{
    let conn = establish_connection();
    use schema::lessons::dsl::*;
    lessons.select(repet_id)
        .filter(id.eq(lesson_id2))
        .load::<Option<i32>>(&conn).unwrap().pop().unwrap().unwrap()


}


//adds a new row at reports and updates corresponding lesson with its report's id
pub fn report_tutor_info<'a>(lesson_id1:i32,tutor_report1:  &'a str, proof1:  &'a str, scholar_adeq1: f32)->Result<i32,Error>{
    let tutor_report1= String::from(tutor_report1);
    let proof1= String::from(proof1);
    let conn = establish_connection();
    let tutor_id1=return_corresponding_tutor_id(lesson_id1);
    use schema::reports::dsl::*;
    let jopa: Result<Report, Error>=insert_into(reports)
        .values((tutor_id.eq(tutor_id1), tutors_report.eq(&tutor_report1), prooflink_photo.eq(&proof1),scholar_adeq.eq(scholar_adeq1)))
        .get_result(&conn);

    match jopa  {
        Ok(a) =>{
            use schema::lessons::dsl::*;
            match diesel::update(lessons.filter(id.eq(lesson_id1)))
            .set(report_id.eq(a.id))
            .execute(&conn) {
                Ok(_)=> {
                    slog_info!(slog_scope::logger(), "report_tutor_info"; "lesson_id"=>lesson_id1,"tutor_report"=>tutor_report1,"proof"=>proof1, "scholar_adeq"=>scholar_adeq1);
                    return Ok(a.id)
                }
                Err(error)=>{
                    slog_error!(slog_scope::logger(), "report_tutor_info failed";  "lesson_id"=>lesson_id1,"tutor_report"=>tutor_report1,"proof"=>proof1,  "scholar_adeq"=>scholar_adeq1,"error" => format!("{}", error));
                    return Err(error)
                }
            }

            },
        Err(error) => {
            slog_error!(slog_scope::logger(), "report_tutor_info failed";  "lesson_id"=>lesson_id1,"tutor_report"=>tutor_report1,"proof"=>proof1,"scholar_adeq"=>scholar_adeq1, "error" => format!("{}", error));
            Err(error)}
    }

}


//unsafe, addlogs
fn return_corresponding_report_id(lesson_id2:i32)->i32{
    let conn = establish_connection();
    use schema::lessons::dsl::*;
    lessons.select(report_id)
        .filter(id.eq(lesson_id2))
        .load::<Option<i32>>(&conn).unwrap().pop().unwrap().unwrap()


}


pub fn report_scholar_info<'a>(lesson_id1:i32,scholars_report1:  &'a str, rating1: f32)->Result<(),Error>{

    let scholars_report1= String::from(scholars_report1);
    let conn = establish_connection();
    let report_id1=return_corresponding_report_id(lesson_id1);
    use schema::reports::dsl::*;
    let jopa=diesel::update(reports.filter(id.eq(report_id1)))
        .set((scholars_report.eq(&scholars_report1),rating.eq(rating1)))
        .execute(&conn);

    match jopa  {
        Ok(_) => {slog_info!(slog_scope::logger(), "report_scholar_info"; "lesson_id"=>lesson_id1, "scholars_report"=>scholars_report1,"rating"=>rating1);
            return Ok(())
        }
        Err(error) => { slog_error!(slog_scope::logger(), "report_scholar_info failed"; "lesson_id"=>lesson_id1, "scholars_report"=>scholars_report1,"rating"=>rating1 , "error" => format!("{}", error));
            Err(error)}
    }

}


//unsafe
pub fn tutor_can_not_teach(telegram_id1: i64, new_subj1: i16 )
                      -> Result<(), Error>
{
    use schema::tutors::dsl::*;
    let conn = establish_connection();
    let loc= tutors.select(list_of_courses)
        .filter(telegram_id.eq(telegram_id1))
        .load::<Vec<i16>>(&conn)
    ;
    let mut a:Vec<i16>=vec![];
    for i in &loc {
        for j in i{
            for k in j{
                if *k!=new_subj1{
                a.push(*k);}
                else {}
            }
        }
    }
    match  diesel::update(tutors.filter(telegram_id.eq(telegram_id1)))
        .set(list_of_courses.eq(a))
        .execute(&conn)
        {
            Ok(_) => {
                slog_info!(slog_scope::logger(), " subject deleted"; "telegram_id" => telegram_id1, "subject"=> new_subj1);
            },
            Err(error) => {
                slog_error!(slog_scope::logger(), "subject deletion failed";
    "telegram_id" => telegram_id1, "subject"=> new_subj1, "error" => format!("{}", error));
                // should be more informative
                return Err(error)
            }
        };
    Ok(())
}

fn get_sch_telegr_id(sch_id2:i32)->i64{
    let conn = establish_connection();

    use schema::scholars::dsl::*;
    scholars.select(telegram_id)
        .filter(id.eq(sch_id2))
        .load::<Option<i64>>(&conn).unwrap().pop().unwrap().unwrap()


    
    
}
//new function
pub fn get_scholar_id_by_lesson(lesson_id2:i32)
   ->i64
{
    let conn = establish_connection();
    use schema::lessons::dsl::*;
   let a:i32= lessons.select(scholar_id)
        .filter(id.eq(lesson_id2))
        .load::<Option<i32>>(&conn).unwrap().pop().unwrap().unwrap();

    get_sch_telegr_id(a)
}

/*pub fn monthly_report()->Result<(),Error> {

    let conn = establish_connection();
    let query: String= String::from(r#"
    SELECT lessons.date,lessons.length,lessons.rating,subjects.name, tutors.name,tutors.faculty, tutors.group, tutors.experience,scholars.name AS "scholar's name", scholars.group AS "scholar's group"
    FROM lessons
    JOIN subjects ON lessons.subject =subjects.id
    JOIN tutors ON lessons.repet_id=tutors.id
    JOIN scholars ON lessons.scholar_id=scholars.id
    WHERE lessons.stage=2 AND EXTRACT(MONTH FROM lessons.date)=07 AND tutors.faculty=2"#);
    let mut users:Vec<User> = diesel::sql_query(query).load(&conn).unwrap();
    let a= users.pop().unwrap().telegram_id;
    println!("{}",a);
    Ok(())
}*/

/*remove_lessons
substract n-th subject from list_of_courses,
update_tutor_group
update_scholar_group
you_have_died        delete tutor
synchronize_ratings        synchronizes rating in reports table with rating in lessons table


*/
