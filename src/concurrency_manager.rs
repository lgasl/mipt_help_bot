use chashmap::CHashMap;
use std::sync::mpsc::{sync_channel, Receiver, SyncSender};
use telegram_bot::types;

pub struct Manager {
    map: CHashMap<types::UserId, SyncSender<types::Update>>,
}

impl Manager {
    pub fn new() -> Self {
        Manager {
            map: CHashMap::new(),
        }
    }

    // return Some(update) if update was not sent
    pub fn try_send(&self, update: types::Update) -> Option<types::Update> {
        let key = match Manager::get_user_id_from_update(&update) {
            Some(value) => value,
            None => return Some(update),
        };

        if let Some(guard) = self.map.get(&key) {
            if guard.send(update).is_err() {
                slog_error!( slog_scope::logger(), "Manager: senderror in last update";
                    "user_id" => format!("{}", key));
            };
            return None;
        }

        Some(update)
    }

    pub fn build_channel(&self, key: &types::UserId) -> Receiver<types::Update> {
        let key = key.clone();

        let (sender, receiver) = sync_channel(10);
        if self.map.insert(key, sender).is_some() {
            slog_error!( slog_scope::logger(), "Manager: Insert an existing key";
                    "key" => format!("{:?}",&key));
            panic!("Manager: Insert an existing key");
        }
        receiver
    }

    pub fn remove_key(&self, key: &types::UserId) {
        let res = self.map.remove(key);
        if res.is_none() {
            slog_error!( slog_scope::logger(), "Deleting a nonexistent key";
                    "key" => format!("{:?}",key));
        }
    }

    fn get_user_id_from_update(update: &types::Update) -> Option<types::UserId> {
        if let types::UpdateKind::Message(message) = &update.kind {
            return Some(message.from.id);
        }
        if let types::UpdateKind::CallbackQuery(query) = &update.kind {
            return Some(query.from.id);
        }
        None
    }
}
