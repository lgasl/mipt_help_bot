use std::env;
use telegram_bot::Api;
use telegram_bot_raw::Request;
use tokio_core::reactor::Core;

pub struct MessageSender {
    core: Core,
    api: Api,
}

impl MessageSender {
    pub fn new() -> Self {
        let core = Core::new().unwrap();
        let handle = core.handle();

        let token = env::var("BOT_TOKEN").expect("BOT_TOKEN must be set");

        let api = Api::configure(&token).build(&handle).unwrap();
        MessageSender { core, api }
    }

    pub fn spawn<Req: Request>(&mut self, request: Req) {
        let future = self.api.send(request);
        self.core
            .run(future)
            .expect("can't run future in MessageSender::spawn");
    }
}
