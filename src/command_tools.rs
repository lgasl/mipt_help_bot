use chrono::NaiveDateTime;
use command::AccessFlag;
use db;
use file_tools::load_file_from_tmp;
use regex::Regex;
use sender::MessageSender;
use std::collections::HashMap;
use std::sync::mpsc::Receiver;
use telegram_bot::types::*;
use telegram_bot::CanSendMessage;

// return None, if update isn't text-message
pub fn get_text_message<'a: 'b, 'b>(update: &'a Update) -> Option<(&'b Message, &'b String)> {
    if let UpdateKind::Message(message) = &update.kind {
        if let MessageKind::Text { ref data, .. } = &message.kind {
            return Some((message, data));
        }
    }
    None
}

pub fn get_text<'a: 'b, 'b>(update: &'a Update) -> Option<&'b String> {
    if let UpdateKind::Message(message) = &update.kind {
        if let MessageKind::Text { ref data, .. } = &message.kind {
            return Some(data);
        }
    }
    None
}

pub fn get_query_data<'a: 'b, 'b>(update: &'a Update) -> Option<&'b String> {
    if let UpdateKind::CallbackQuery(query) = &update.kind {
        return Some(&query.data);
    }
    None
}

pub fn get_query_message(update: &Update) -> Option<&Message> {
    if let UpdateKind::CallbackQuery(query) = &update.kind {
        return Some(&query.message);
    }
    None
}

pub fn get_query_message_and_data<'a: 'b, 'b>(
    update: &'a Update,
) -> Option<(&Message, &'b String)> {
    if let UpdateKind::CallbackQuery(query) = &update.kind {
        return Some((&query.message, &query.data));
    }
    None
}

pub fn get_query_from_and_message_and_data<'a: 'b, 'b>(
    update: &'a Update,
) -> Option<(UserId, &Message, &'b String)> {
    if let UpdateKind::CallbackQuery(query) = &update.kind {
        return Some((query.from.id, &query.message, &query.data));
    }
    None
}

pub fn is_break(update: &Update) -> bool {
    match get_text(update) {
        Some(data) => {
            let re = Regex::new(r"/break").unwrap();
            re.is_match(data)
        }
        None => false,
    }
}

pub fn is_button_break(update: &Update) -> bool {
    match get_query_data(update) {
        Some(data) => data.eq("br"),
        None => false,
    }
}

pub fn is_button_back(update: &Update) -> bool {
    match get_query_data(update) {
        Some(data) => data.eq("back"),
        None => false,
    }
}

pub fn is_confirmation(update: &Update) -> bool {
    match get_text(update) {
        Some(data) => {
            let re = Regex::new(r"y|yes|д|да|ok").unwrap();
            re.is_match(data)
        }
        None => false,
    }
}

pub fn is_button_confirmation(update: &Update) -> bool {
    match get_query_data(update) {
        Some(data) => data.eq("ok"),
        None => false,
    }
}

pub fn is_name(data: &str) -> bool {
    let pattern = r"\s*\w+\s+\w+(\s+\w+)*";
    let regex = Regex::new(pattern).expect("Invalid common command pattern");
    regex.is_match(data)
}

// TODO enable ****-groups
pub fn group_to_faculty(group: i16) -> i16 {
    (group / 10) % 10
}

pub fn keyboard_with_text<'a>(
    chat: &ChatId,
    text: &'a str,
    keyboard: InlineKeyboardMarkup,
) -> SendMessage<'a> {
    let mut message: SendMessage = chat.text(text);
    message.reply_markup(keyboard);
    message
}

pub fn load_sections() -> Vec<Section> {
    let json_data = load_file_from_tmp("Sections.json");
    let json_data = match json_data {
        Ok(data) => data,
        Err(err) => panic!("{}", err),
    };
    match serde_json::from_str(&json_data) {
        Ok(sections) => sections,
        Err(err) => panic!("{}", err),
    }
}

pub fn load_subjects_map() -> HashMap<u8, String> {
    let json_data = load_file_from_tmp("Subjects.json");
    let json_data = match json_data {
        Ok(data) => data,
        Err(err) => panic!(err),
    };
    match serde_json::from_str(&json_data) {
        Ok(subjects) => subjects,
        Err(err) => panic!("{}", err),
    }
}

pub fn get_lesson_offer<'a: 'b, 'b>(
    tutor_id: UserId,
    subject: &'a str,
    info: &'a str,
    lesson_id: i32,
) -> SendMessage<'b> {
    let chat: ChatId = tutor_id.into();
    let mut message = chat.text(format!(
        "Вам предложен урок:\n\
         предмет: {}, описание: {}",
        subject, info
    ));
    let mut keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup::new();
    let accept_text = "che ".to_string() + &lesson_id.to_string();
    let reject_text = "rej ".to_string() + &lesson_id.to_string();
    let accept = InlineKeyboardButton::callback("Отслеживать", accept_text);
    let reject = InlineKeyboardButton::callback("Отказаться", reject_text);
    keyboard.add_row(vec![accept, reject]);
    message.reply_markup(keyboard);

    message
}
// TODO delete chat
pub fn check_lesson_offer<'a: 'b, 'b>(
    tutor_id: UserId,
    message_id: MessageId,
    chat: ChatId,
    lesson_id: i32,
    sender: &mut MessageSender,
) {
    let mut keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup::new();
    let accept_text = "acc ".to_string() + &lesson_id.to_string();
    let reject_text = "rej ".to_string() + &lesson_id.to_string();
    let accept = InlineKeyboardButton::callback("Принять", accept_text);
    let reject = InlineKeyboardButton::callback("Отказаться", reject_text);
    keyboard.add_row(vec![accept, reject]);
    sender.spawn(EditMessageReplyMarkup::new(
        chat,
        message_id,
        Some(keyboard),
    ));
}

fn get_custom_keyboard<T: KeyboardImpl>(sections: &Vec<T>) -> InlineKeyboardMarkup {
    let mut keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup::new();
    let mut iterator = sections.iter();
    let size = sections.len();
    const BUTTONS_PER_ROW: usize = 3;
    let rows = (size + BUTTONS_PER_ROW - 1) / BUTTONS_PER_ROW;
    for _row in 0..rows {
        let mut buttons_row = Vec::new();
        for _i in 0..BUTTONS_PER_ROW {
            let item = iterator.next();
            match item {
                Some(value) => {
                    buttons_row.push(InlineKeyboardButton::callback(
                        value.to_name(),
                        value.to_number().to_string(),
                    ));
                }
                None => continue,
            }
        }
        keyboard.add_row(buttons_row);
    }

    keyboard
}

pub fn get_sections_keyboard(sections: &Vec<Section>) -> InlineKeyboardMarkup {
    let mut keyboard = get_custom_keyboard(sections);
    keyboard.add_row(vec![InlineKeyboardButton::callback("Готово", "br")]);
    keyboard
}

pub fn get_subjects_keyboard(subjects: &Vec<Subject>) -> InlineKeyboardMarkup {
    let mut keyboard = get_custom_keyboard(subjects);
    keyboard.add_row(vec![
        InlineKeyboardButton::callback("Готово", "br"),
        InlineKeyboardButton::callback("Назад", "back"),
    ]);
    keyboard
}

pub fn get_ok_cancel_keyboard() -> InlineKeyboardMarkup {
    let mut keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup::new();
    let ok = InlineKeyboardButton::callback("ok", "ok");
    let cancel = InlineKeyboardButton::callback("Отмена", "br");
    keyboard.add_row(vec![ok, cancel]);

    keyboard
}

pub fn get_edit_keyboard<'a: 'b, 'b>(
    chat_id: &ChatId,
    message_id: &MessageId,
    text: &'a str,
    keyboard: InlineKeyboardMarkup,
) -> EditMessageText<'b> {
    let mut edit_req = EditMessageText::new(chat_id, message_id, text);
    edit_req.reply_markup(keyboard);
    edit_req
}

pub fn get_edit_empty_keyboard<'a: 'b, 'b>(
    chat_id: &ChatId,
    message_id: &MessageId,
    text: &'a str,
) -> EditMessageText<'b> {
    let edit_req = EditMessageText::new(chat_id, message_id, text);
    edit_req
}

pub fn get_lesson_tutors_report_req<'a>(user_id: UserId, lesson_id: i32) -> SendMessage<'a> {
    let chat_id: ChatId = user_id.into();
    let scholar_id = db::get_scholar_id_by_lesson(lesson_id);
    let mut message = chat_id.text(format!("Вы приняли запрос на урок {}. \
                                            Cвяжитесь пожалуйста с [учеником](tg://user?id={}) \
                                            Пожалуйста, оставьте отчёт после того, как пройдет урок. \
                                            Ученик сможет оставить отзыв только после Вашего отчета",
                                           lesson_id, scholar_id));
    message.parse_mode(ParseMode::Markdown);

    let mut keyboard = InlineKeyboardMarkup::new();
    // tre ~ tutor_report
    let report_text = "tre ".to_string() + &lesson_id.to_string();
    keyboard.add_row(vec![InlineKeyboardButton::callback(
        "Отчет",
        report_text,
    )]);
    message.reply_markup(keyboard);
    message
}

pub fn get_lesson_scholar_report_req(chat_id: &ChatId, lesson_id: i32) -> SendMessage {
    let mut message = chat_id.text(format!(
        "Ваш урок {} прошел. \
         Пожалуйста, оставьте отзыв о преподавателе.",
        lesson_id
    ));

    let mut keyboard = InlineKeyboardMarkup::new();
    // ure ~ user_report
    let report_text = "ure ".to_string() + &lesson_id.to_string();
    keyboard.add_row(vec![InlineKeyboardButton::callback(
        "Отзыв",
        report_text,
    )]);
    message.reply_markup(keyboard);
    message
}

#[derive(Serialize, Deserialize)]
pub struct Section {
    name: String,
    number: u8,
    pub sub_themes: Vec<Subject>,
}

#[derive(Serialize, Deserialize)]
pub struct Subject {
    name: String,
    number: u8,
}

pub trait KeyboardImpl {
    fn to_name(&self) -> &str;
    fn to_number(&self) -> u8;
}

impl KeyboardImpl for Section {
    fn to_name(&self) -> &str {
        &self.name
    }

    fn to_number(&self) -> u8 {
        self.number
    }
}

impl KeyboardImpl for Subject {
    fn to_name(&self) -> &str {
        &self.name
    }

    fn to_number(&self) -> u8 {
        self.number
    }
}

// name: String, telegram_id: i64, telegram_name: &str, faculty: i16,
// group: i16, site: &str
pub fn get_user_info_dialog<'a>(
    user_id: UserId,
    receiver: &Receiver<Update>,
    sender: &mut MessageSender,
) -> Option<(String, i64, i16, i16, &'a str)> {
    let chat: ChatId = user_id.into();

    let telegram_id: i64 = user_id.into();

    // user name
    let mut name = String::new();
    while name.is_empty() {
        sender.spawn(chat.text(format!(
            "Напишите своё настоящее ФИО"
        )));

        let update = receiver.recv().unwrap();
        if is_break(&update) {
            return None;
        }

        match get_text(&update) {
            Some(text) => {
                if !is_name(text) {
                    continue;
                }
                name = text.clone();
            }
            None => continue,
        }
    }

    let mut course: i16 = 0;
    while course == 0 {
        sender.spawn(chat.text(format!("Напишите номер курса, на котором учитесь")));

        let update = receiver.recv().unwrap();
        if is_break(&update) {
            return None;
        }

        match get_text(&update) {
            Some(text) => {
                course = match text.parse() {
                    Ok(value) => value,
                    Err(_) => continue,
                };
            }
            None => continue,
        }
    }

    // user group/faculty
    let mut group: i16 = 0;
    let mut faculty: i16 = 0;
    while group == 0 {
        sender.spawn(chat.text(format!("Напишите номер группы")));

        let update = receiver.recv().unwrap();
        if is_break(&update) {
            return None;
        }

        match get_text(&update) {
            Some(text) => {
                group = match text.parse() {
                    Ok(value) => value,
                    Err(_) => continue,
                };
                faculty = group_to_faculty(group);
            }
            None => continue,
        }
    }

    Some((name, telegram_id, faculty, group, ""))
}

pub fn check_user_info(
    chat: &ChatId,
    name: &str,
    _faculty: i16,
    group: i16,
    receiver: &Receiver<Update>,
    sender: &mut MessageSender,
) -> bool {
    let text = format!(
        "Проверьте правильность данных и нажмите Ok \
         имя: {}, номер группы: {}",
        name, group
    );
    let message = keyboard_with_text(chat, &text, get_ok_cancel_keyboard());
    sender.spawn(message);

    let update = receiver.recv().unwrap();
    if let Some(message) = get_query_message(&update) {
        sender.spawn(DeleteMessage::new(chat, &message));
    };
    is_button_confirmation(&update)
}

pub fn get_subjects(
    chat: ChatId,
    receiver: &Receiver<Update>,
    sender: &mut MessageSender,
) -> Vec<u8> {
    //let mut is_ready = false;
    let mut subjects = Vec::new();
    let subjects_map = load_subjects_map();

    let sections: &Vec<Section> = &load_sections();
    let keyboard: InlineKeyboardMarkup = get_sections_keyboard(sections);
    let keyboard_clone = keyboard.clone();
    let message: SendMessage =
        keyboard_with_text(&chat, "Выберите раздел", keyboard_clone);
    sender.spawn(message);

    let mut stage = 0;
    while stage != 2 {
        let update = receiver.recv().unwrap();
        if is_button_break(&update) || is_break(&update) {
            if let Some(message) = get_query_message(&update) {
                sender.spawn(DeleteMessage::new(chat, &message));
            }
            return subjects;
        }
        if is_button_back(&update) {
            if let Some(message) = get_query_message(&update) {
                let keyboard_clone = keyboard.clone();
                sender.spawn(get_edit_keyboard(
                    &chat,
                    &message.to_message_id(),
                    "Выберите раздел",
                    keyboard_clone,
                ));
                stage = 0;
            }
            continue;
        }

        if let Some((message, data)) = get_query_message_and_data(&update) {
            if stage == 0 {
                let group: usize = match data.parse() {
                    Ok(value) => value,
                    Err(_) => continue,
                };
                let subjects = &sections[group].sub_themes;

                let mut keyboard: InlineKeyboardMarkup = get_subjects_keyboard(subjects);
                sender.spawn(get_edit_keyboard(
                    &chat,
                    &message.to_message_id(),
                    "Выберите предмет",
                    keyboard,
                ));
                stage = 1;
                continue;
            } else if stage == 1 {
                let chosen_subject: u8 = match data.parse() {
                    Ok(value) => value,
                    Err(_) => continue,
                };
                subjects.push(chosen_subject);
                sender.spawn(DeleteMessage::new(chat, &message));
                let subject_text = match subjects_map.get(&chosen_subject) {
                    Some(value) => value,
                    None => return subjects,
                };
                sender.spawn(chat.text(format!(
                    "Вам успешно добавлен предмет {}",
                    subject_text
                )));
                let keyboard_clone = keyboard.clone();
                let message: SendMessage =
                    keyboard_with_text(&chat, "Выберите раздел", keyboard_clone);
                sender.spawn(message);
                stage = 0;
            }
        } else {
            continue;
        }
    }

    subjects
}

pub fn get_tutor_report(
    lesson_id: i32,
    chat: ChatId,
    receiver: &Receiver<Update>,
    sender: &mut MessageSender,
) {
    let date: NaiveDateTime; // = NaiveDate::from_ymd(2015, 6, 3).and_hms(23, 56, 4);
    loop {
        sender.spawn(chat.text("Пожалуйста, напишите дату урока в формате YYYY-MM-DD HH"));
        let update = receiver.recv().unwrap();
        if let Some(text) = get_text(&update) {
            let text = format!("{}:00:00", text);
            match NaiveDateTime::parse_from_str(&text, "%Y-%m-%d %H:%M:%S") {
                Ok(value) => {
                    date = value;
                    break;
                }
                Err(err) => {
                    println!("{}", err);
                    continue;
                }
            };
        }
    }

    let time: f32;
    loop {
        sender.spawn(
            chat.text("Пожалуйста, напишите время урока в часах"),
        );
        let update = receiver.recv().unwrap();
        if let Some(text) = get_text(&update) {
            match text.parse() {
                Ok(value) => {
                    time = value;
                    break;
                }
                Err(_) => continue,
            }
        }
    }

    sender.spawn(
        chat.text("Пожалуйста, опишите своё мнение об ученике"),
    );
    check_db_result(db::lesson_update2(lesson_id, date, time));

    let mut report = String::new();
    while report.is_empty() {
        let update = receiver.recv().unwrap();
        if let Some(text) = get_text(&update) {
            report = text.clone();
        }
    }

    //TODO add proof
    check_db_result(db::report_tutor_info(lesson_id, &report, "", 0.));
    println!("reported");
}

pub fn get_scholar_report(
    lesson_id: i32,
    chat: ChatId,
    receiver: &Receiver<Update>,
    sender: &mut MessageSender,
) {
    let grade: f32;
    loop {
        sender.spawn(chat.text("Пожалуйста, оцените навыки преподавателя по 10-балльной шкале"));
        let update = receiver.recv().unwrap();
        if let Some(text) = get_text(&update) {
            match text.parse() {
                Ok(value) => {
                    grade = value;
                    break;
                }
                Err(_) => continue,
            }
        }
    }

    sender.spawn(chat.text("Пожалуйста, опишите своё мнение о преподавателе более\
                                    развернуто, парой предложений"));

    let mut report = String::new();
    while report.is_empty() {
        let update = receiver.recv().unwrap();
        if let Some(text) = get_text(&update) {
            report = text.clone();
        }
    }

    check_db_result(db::report_scholar_info(lesson_id, &report, grade));
}

pub fn remove_checked_messages(tutors: Vec<i64>, messages: Vec<i64>, sender: &mut MessageSender) {
    for i in 0..tutors.len() {
        let user_id = UserId::new(tutors[i]);
        let message_id = MessageId::new(messages[i]);
        let chat: ChatId = user_id.into();
        sender.spawn(DeleteMessage::new(chat, message_id));
    }
}

pub fn get_menu<'a>(user_id: UserId) -> SendMessage<'a> {
    let chat: ChatId = user_id.into();
    let mut message = chat.text("Выберите действие");
    let mut keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup::new();
    let access = db::get_user_access_rights(i64::from(user_id));
    if (access & AccessFlag::Tutor as i16) == 0 {
        let button_reg_tutor = InlineKeyboardButton::callback(
            "Зарегистрироваться, как преподаватель",
            "reg_tutor",
        );
        keyboard.add_row(vec![button_reg_tutor]);
    }

    if (access & AccessFlag::Scholar as i16) == 0 {
        let button_reg_scholar = InlineKeyboardButton::callback(
            "Зарегистрироваться, как ученик",
            "reg_scholar",
        );
        keyboard.add_row(vec![button_reg_scholar]);
    } else {
        let button_want = InlineKeyboardButton::callback("Запрос урока", "want");
        keyboard.add_row(vec![button_want]);
    }

    let button_cancel = InlineKeyboardButton::callback("Отмена", "cancel");
    keyboard.add_row(vec![button_cancel]);

    message.reply_markup(keyboard);
    message
}

pub fn check_db_result<T, Error: std::error::Error>(result: Result<T, Error>) {
    match result {
        Ok(_) => (),
        Err(err) => slog_error!(slog_scope::logger(), "DB error: {}", err),
    }
}
