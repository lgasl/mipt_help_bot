use regex::{Captures, Regex};
use std::sync::mpsc::Receiver;
use std::sync::Arc;
use std::thread;
use telegram_bot::types;

use concurrency_manager::Manager;
//use command_implementation::add_commands;
use db;

pub struct Command {
    name: &'static str,
    args_pattern: &'static str,
    args_regex: Regex,
    action: fn(
        user_id: types::UserId,
        args: Captures,
        update: types::Update,
        receiver: Option<Receiver<types::Update>>,
    ),
    is_dialog: bool,
    access: AccessFlag,
}

impl Command {
    pub fn new(
        name: &'static str,
        args_pattern: &'static str,
        is_dialog: bool,
        access: AccessFlag,
        action: fn(
            user_id: types::UserId,
            args: Captures,
            update: types::Update,
            receiver: Option<Receiver<types::Update>>,
        ),
    ) -> Self {
        let args_regex = Regex::new(args_pattern).expect(&format!("bad pattern for {}", name));
        Command {
            name,
            args_pattern,
            args_regex,
            action,
            access,
            is_dialog,
        }
    }

    fn eq(&self, text: &str) -> bool {
        text.eq(self.name)
    }

    fn is_args_valid<'a: 'b, 'b>(&self, args: &'a str) -> bool {
        self.args_regex.is_match(args)
    }

    fn is_access(&self, user_id: types::UserId) -> bool {
        let telegram_id: types::Integer = user_id.into();
        let rights = db::get_user_access_rights(telegram_id) + 0b10000000;
        let access = self.access as i16;
        (access & rights) != 0
    }
}

pub struct CommandList {
    commands: Vec<Command>,
    manager: Arc<Manager>,
    command_regex: Regex,
    catch_callback_query: fn(update: types::Update, manager: Arc<Manager>),
}

impl CommandList {
    pub fn new(
        manager: &Arc<Manager>,
        add_commands: fn() -> Vec<Command>,
        catch_callback_query: fn(types::Update, Arc<Manager>),
    ) -> Self {
        let manager = manager.clone();
        let commands: Vec<Command> = add_commands();
        let pattern = r"/(?P<command>\w+)\s*(?P<args>$|[\w\s?]+)";
        let command_regex = Regex::new(pattern).expect("Invalid common command pattern");
        CommandList {
            commands,
            manager,
            command_regex,
            catch_callback_query,
        }
    }

    pub fn try_run_command(
        &self,
        data: &str,
        update: types::Update,
        user_id: types::UserId,
    ) -> Result<(), CommandError> {
        let captures = self.get_candidate(data)?;
        let command = self.find_command(&captures["command"])?;
        if !command.is_access(user_id) {
            return Err(CommandError::NoAccess);
        };

        if !command.is_args_valid(&captures["args"]) {
            return Err(CommandError::InvalidArgs);
        }

        let raw_args = &captures["args"];
        self.run(command, raw_args, update, user_id);
        Ok(())
    }

    // None if data clearly is not command
    // capture for (command, args)
    fn get_candidate<'a: 'b, 'b>(&self, data: &'a str) -> Result<Captures<'b>, CommandError> {
        if let Some(captures) = self.command_regex.captures(data) {
            return Ok(captures);
        }

        Err(CommandError::NotACommand)
    }

    fn find_command(&self, candidate: &str) -> Result<&Command, CommandError> {
        for command in self.commands.iter() {
            if command.eq(candidate) {
                return Ok(command);
            }
        }

        Err(CommandError::InvalidCommand)
    }

    // return captures for args
    fn parse_args<'a: 'b, 'b>(args: &'a str, pattern: &str) -> Captures<'b> {
        let regex = Regex::new(pattern).expect(&format!("bad pattern"));
        let captures = regex.captures(args);
        if let Some(captures) = captures {
            return captures;
        }
        panic!("bad args")
    }

    fn run(
        &self,
        command: &Command,
        raw_args: &str,
        update: types::Update,
        user_id: types::UserId,
    ) {
        // Prepare
        let (receiver, manager) = if command.is_dialog {
            (
                Some(self.manager.build_channel(&user_id)),
                Some(self.manager.clone()),
            )
        } else {
            (None, None)
        };

        let action = command.action;
        let is_dialog = command.is_dialog;
        let pattern = command.args_pattern.to_string();
        let raw_args = raw_args.to_string();

        // run
        thread::spawn(move || {
            let pattern = pattern;
            let args = CommandList::parse_args(&raw_args, &pattern);
            action(user_id, args, update, receiver);
            if is_dialog {
                if let Some(ref manager) = manager {
                    manager.remove_key(&user_id);
                } else {
                    panic!("was used nonexistent manager. How?")
                }
            }
        });
    }

    pub fn try_catch_callback_query(&self, update: types::Update) -> Option<types::Update> {
        if let types::UpdateKind::CallbackQuery(_) = &update.kind {
            let manager = self.manager.clone();
            let catch_callback_query = self.catch_callback_query;
            thread::spawn(move || {
                catch_callback_query(update, manager);
            });
            return None;
        }
        Some(update)
    }
}

pub enum CommandError {
    InvalidCommand,
    InvalidArgs,
    NotACommand,
    NoAccess,
}

#[derive(Clone, Copy)]
pub enum AccessFlag {
    Any = 0b10000000,
    Admin = 0b00000001,
    Moderator = 0b00000010,
    Scholar = 0b00000100,
    Tutor = 0b00001000,
}
