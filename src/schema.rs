table! {
    checks (id) {
        tutors -> Nullable<Array<Int8>>,
        messages -> Nullable<Array<Int8>>,
        lesson_id -> Nullable<Int4>,
        id -> Int4,
    }
}

table! {
    lessons (id) {
        repet_id -> Nullable<Int4>,
        scholar_id -> Nullable<Int4>,
        date -> Nullable<Timestamp>,
        length -> Nullable<Float4>,
        subject -> Nullable<Int2>,
        stage -> Nullable<Int2>,
        report_id -> Nullable<Int4>,
        id -> Int4,
    }
}

table! {
    reports (id) {
        scholars_report -> Nullable<Text>,
        tutors_report -> Nullable<Text>,
        prooflink_photo -> Nullable<Varchar>,
        rating -> Nullable<Float4>,
        tutor_id -> Int4,
        scholar_adeq -> Nullable<Float4>,
        id -> Int4,
    }
}

table! {
    scholars (id) {
        name -> Nullable<Varchar>,
        id -> Int4,
        telegram_id -> Nullable<Int8>,
        faculty -> Nullable<Int2>,
        group -> Nullable<Int2>,
        site -> Nullable<Varchar>,
        mail -> Nullable<Varchar>,
        adeq -> Nullable<Int2>,
        telegram_name -> Nullable<Varchar>,
    }
}

table! {
    subjects (id) {
        id -> Int4,
        name -> Nullable<Varchar>,
    }
}

table! {
    tutors (id) {
        name -> Varchar,
        faculty -> Nullable<Int2>,
        group -> Int2,
        telegram_name -> Varchar,
        telegram_id -> Int8,
        site -> Nullable<Varchar>,
        list_of_courses -> Array<Int2>,
        list_of_recos -> Array<Int2>,
        rating -> Float4,
        active -> Bool,
        mail -> Nullable<Varchar>,
        experience -> Int4,
        id -> Int4,
    }
}

table! {
    users (id) {
        id -> Int4,
        telegram_id -> Int8,
        groups -> Int2,
    }
}

allow_tables_to_appear_in_same_query!(
    checks,
    lessons,
    reports,
    scholars,
    subjects,
    tutors,
    users,
);
